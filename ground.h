#ifndef GROUND_H
#define GROUND_H

#define WORLD_SIZE 100.0F
#define GROUND_COLOR 0.2F

#include "renderobject.h"

class Ground : public RenderObject
{
public:
    Ground(QOpenGLFunctions_4_4_Core *gl);
};

#endif // GROUND_H
