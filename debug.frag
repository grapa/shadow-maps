#version 420

in vec2 uv_FS;

out vec4 frag;

uniform float znear;
uniform float zfar;

layout(binding = 0) uniform sampler2D shadowMap;

float linearize(float depth)
{
    return (-zfar * znear / (depth * (zfar - znear) - zfar)) / zfar;
}

void main(void)
{
    float depthValue = texture(shadowMap, uv_FS).r;
    frag = vec4(vec3(linearize(depthValue)), 1);
}
