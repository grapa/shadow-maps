#ifndef CUBE_H
#define CUBE_H

#define CUBE_SIZE 5

#include "renderobject.h"
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLShaderProgram>

class Cube : public RenderObject
{
public:
    Cube(QOpenGLFunctions_4_4_Core *gl);
};

#endif // CUBE_H
