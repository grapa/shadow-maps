#-------------------------------------------------
#
# Project created by QtCreator 2019-07-07T13:44:57
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = shadowmaps
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        camera.cpp \
        cube.cpp \
        debuglogger.cpp \
        errorchecker.cpp \
        ground.cpp \
        helloglwidget.cpp \
        lightsource.cpp \
        main.cpp \
        mainwindow.cpp \
        renderobject.cpp \
        sceneobject.cpp \
        shadowmap.cpp

HEADERS += \
        camera.h \
        cube.h \
        debuglogger.h \
        errorchecker.h \
        ground.h \
        helloglwidget.h \
        lightsource.h \
        mainwindow.h \
        renderobject.h \
        sceneobject.h \
        shadowmap.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    debug.frag \
    debug.vert \
    normal.frag \
    normal.vert \
    pcf.frag \
    pcf.vert \
    shadowmap.frag \
    shadowmap.vert \
    vsm.frag \
    vsm.vert

RESOURCES += \
    resources.qrc
