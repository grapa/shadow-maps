#ifndef HELLOGLWIDGET_H
#define HELLOGLWIDGET_H

// #define DEBUG_SHADOW_MAP
#define Z_NEAR 0.1F
#define Z_FAR 100.0F
#define SHADER_MODE_NORMAL 0
#define SHADER_MODE_PCF 1
#define SHADER_MODE_VSM 2

#include "cube.h"
#include "camera.h"
#include "ground.h"
#include "lightsource.h"
#include "debuglogger.h"
#include "errorchecker.h"
#include <QTimer>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>

class HelloGLWidget : public QOpenGLWidget, protected ErrorChecker
{
    Q_OBJECT

public:
    HelloGLWidget(QWidget *parent = nullptr);
    ~HelloGLWidget() override = default;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

public slots:
    void handleTimeout();
    void normal();
    void pcf();
    void vsm();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int, int) override {}
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    void resize(int width, int height);
    QOpenGLShaderProgram* createShader(const QString& vertName, const QString& fragName);
    QOpenGLShaderProgram* getRenderShader();
    QMatrix4x4 getLightSpace(LightSource *lightSource) const;
    void render(QOpenGLShaderProgram *shader);

    QTimer m_timer;

    DebugLogger *m_debugLogger;

    Camera m_camera = Camera(QVector3D(0, 5, 0));
    QMatrix4x4 m_p;

    QOpenGLShaderProgram *m_shadowMapShader;
    QOpenGLShaderProgram *m_normalShader;
    QOpenGLShaderProgram *m_pcfShader;
    QOpenGLShaderProgram *m_vsmShader;
    int m_shaderMode = SHADER_MODE_NORMAL;

    LightSource **m_lightSources;
    size_t m_lightSourceCount = 2;
    Ground *m_ground;
    Cube **m_cubes;
    size_t m_cubeCount = 2;

    QPoint m_lastMousePos;
    bool m_mousePressed = false;

    bool m_wDown = false;
    bool m_aDown = false;
    bool m_sDown = false;
    bool m_dDown = false;
    bool m_xDown = false;
    bool m_spaceDown = false;

#ifdef DEBUG_SHADOW_MAP
    QOpenGLShaderProgram *m_debugShader;

    GLuint m_vao;
    GLuint m_vbo;
    GLuint m_ibo;
#endif
};

#endif // HELLOGLWIDGET_H
