#include "ground.h"
#include <iostream>

const unsigned int numTris = 2;
const unsigned int numVerts = 4;

const GLfloat vertexPosition[3 * numVerts] =
{
    -WORLD_SIZE, 0, -WORLD_SIZE,
    -WORLD_SIZE, 0, WORLD_SIZE,
    WORLD_SIZE, 0, -WORLD_SIZE,
    WORLD_SIZE, 0, WORLD_SIZE
};

const GLuint vertexIndex[3 * numTris] =
{
    0, 1, 2,
    1, 3, 2
};

const GLfloat normals[3 * numVerts] =
{
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0
};

const GLfloat colors[3 * numVerts] =
{
    GROUND_COLOR, GROUND_COLOR, GROUND_COLOR,
    GROUND_COLOR, GROUND_COLOR, GROUND_COLOR,
    GROUND_COLOR, GROUND_COLOR, GROUND_COLOR,
    GROUND_COLOR, GROUND_COLOR, GROUND_COLOR
};

Ground::Ground(QOpenGLFunctions_4_4_Core *gl)
    : RenderObject(gl, numTris, numVerts, vertexPosition,
                   vertexIndex, normals, colors) { }
