#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "helloglwidget.h"
#include <QMainWindow>
#include <QtWidgets>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() = default;

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    HelloGLWidget *m_glWidget;
};

#endif // MAINWINDOW_H
