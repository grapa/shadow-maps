#include "shadowmap.h"

ShadowMap::ShadowMap(QOpenGLFunctions_4_4_Core *gl, GLuint defaultFBO, unsigned int index)
    : SceneObject(gl), m_index(index)
{
    m_gl->glGenFramebuffers(1, &m_fbo);

    m_gl->glGenTextures(1, &m_texture);
    m_gl->glActiveTexture(GL_TEXTURE0 + m_index);
    m_gl->glBindTexture(GL_TEXTURE_2D, m_texture);
    m_gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    m_gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_gl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    m_gl->glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

    m_gl->glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    m_gl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_texture, 0);
    m_gl->glDrawBuffer(GL_NONE);
    m_gl->glReadBuffer(GL_NONE);
    m_gl->glBindFramebuffer(GL_FRAMEBUFFER, defaultFBO);
}

void ShadowMap::bind()
{
    m_gl->glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void ShadowMap::bindTexture()
{
    m_gl->glActiveTexture(GL_TEXTURE0 + m_index);
    m_gl->glBindTexture(GL_TEXTURE_2D, m_texture);
}
