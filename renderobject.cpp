#include "renderobject.h"

RenderObject::RenderObject(QOpenGLFunctions_4_4_Core *gl, const unsigned int numTris,
                           const unsigned int numVerts, const GLfloat *vertexPosition,
                           const GLuint *vertexIndex, const GLfloat *normals,
                           const GLfloat *colors)
    : SceneObject(gl), m_numTris(static_cast<int>(numTris))
{
    m_gl->glGenVertexArrays(1, &m_vao);
    m_gl->glBindVertexArray(m_vao);

    m_gl->glGenBuffers(1, &m_vbo);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    m_gl->glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    m_gl->glEnableVertexAttribArray(0);
    m_gl->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_gl->glGenBuffers(1, &m_nbo);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_nbo);
    m_gl->glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), normals, GL_STATIC_DRAW);
    m_gl->glEnableVertexAttribArray(1);
    m_gl->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_gl->glGenBuffers(1, &m_cbo);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_cbo);
    m_gl->glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), colors, GL_STATIC_DRAW);
    m_gl->glEnableVertexAttribArray(2);
    m_gl->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_gl->glGenBuffers(1, &m_ibo);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    m_gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numTris * sizeof(GLuint), vertexIndex, GL_STATIC_DRAW);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    m_gl->glBindVertexArray(0);
}

void RenderObject::translate(float x, float y, float z)
{
    m_m.translate(x, y, z);
}

void RenderObject::scale(float x, float y, float z)
{
    m_m.scale(x, y, z);
}

void RenderObject::rotate(float angle, float x, float y, float z)
{
    m_m.rotate(angle, x, y, z);
}

void RenderObject::render(QOpenGLShaderProgram *shader)
{
    shader->setUniformValue("m", getModelMatrix());

    m_gl->glBindVertexArray(m_vao);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    m_gl->glDrawElements(GL_TRIANGLES, 3 * m_numTris, GL_UNSIGNED_INT, nullptr);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    m_gl->glBindVertexArray(0);
}

QMatrix4x4 RenderObject::getModelMatrix() const
{
    return m_m;
}

QVector3D RenderObject::getPosition() const
{
    QVector4D pos(0, 0, 0, 1);
    pos = getModelMatrix() * pos;
    return QVector3D(pos);
}
