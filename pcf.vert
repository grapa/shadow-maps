#version 400

layout(location = 0) in vec3 pos_VS;
layout(location = 1) in vec3 normal_VS;
layout(location = 2) in vec3 color_VS;

out vec3 worldPos_FS;
out vec4 lightSpacePos0_FS;
out vec4 lightSpacePos1_FS;
out vec3 worldNormal_FS;
out vec3 color_FS;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 ls0;
uniform mat4 ls1;

void main(void)
{
    vec4 worldPos = m * vec4(pos_VS, 1);
    worldPos_FS = vec3(worldPos);
    lightSpacePos0_FS = ls0 * worldPos;
    lightSpacePos1_FS = ls1 * worldPos;
    worldNormal_FS = transpose(inverse(mat3(m))) * normal_VS;
    color_FS = color_VS;
    gl_Position = (p * v) * worldPos;
}
