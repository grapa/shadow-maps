#version 400

layout(location = 0) in vec3 pos_VS;

uniform mat4 m;
uniform mat4 ls;

void main(void)
{
    gl_Position = (ls * m) * vec4(pos_VS, 1);
}
