#version 400

layout(location = 0) in vec3 pos_VS;

out vec2 uv_FS;

void main(void)
{
    gl_Position = vec4(pos_VS, 1);
    uv_FS = (pos_VS.xy + vec2(1)) / 2.0;
}
