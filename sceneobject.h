#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <QOpenGLFunctions_4_4_Core>

class SceneObject
{
public:
    SceneObject(QOpenGLFunctions_4_4_Core *gl);
    virtual ~SceneObject() = default;

protected:
    QOpenGLFunctions_4_4_Core *m_gl;
};

#endif // SCENEOBJECT_H
