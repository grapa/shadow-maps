#ifndef LIGHTSOURCE_H
#define LIGHTSOURCE_H

#include "shadowmap.h"
#include "sceneobject.h"
#include <QVector3D>
#include <QMatrix4x4>

class LightSource : public SceneObject
{
public:
    LightSource(QOpenGLFunctions_4_4_Core *gl, GLuint defaultFBO,
                QVector3D position, QVector3D lookAt, unsigned int index);

    QVector3D getPosition() const;
    QMatrix4x4 getViewMatrix() const;
    ShadowMap *getShadowMap() const;

private:
    QVector3D m_position;
    QVector3D m_lookAt;

    ShadowMap *m_shadowMap;
};

#endif // LIGHTSOURCE_H
