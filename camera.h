#ifndef CAMERA_H
#define CAMERA_H

#include <QVector2D>
#include <QMatrix4x4>

class Camera
{
public:
    Camera(QVector3D point);
    ~Camera() = default;

    QMatrix4x4 getViewMatrix() const;

    void move(bool w, bool a, bool s, bool d, bool x, bool space);
    void rotate(QVector2D from, QVector2D to);

    QVector3D getPosition() const;

private:
    QVector3D m_position;
    QVector3D m_lookAt;
    QVector3D m_up = QVector3D(0, 1, 0);

    const float m_distance = 3;
    const float m_mouseSpeed = 0.002F;
    const float m_moveSpeed = 0.5F;
};

#endif // CAMERA_H
