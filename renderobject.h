#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include "sceneobject.h"
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>

class RenderObject : public SceneObject
{
public:
    RenderObject(QOpenGLFunctions_4_4_Core *gl, const unsigned int numTris,
                 const unsigned int numVerts, const GLfloat *vertexPosition,
                 const GLuint *vertexIndex, const GLfloat *normals,
                 const GLfloat *colors);
    virtual ~RenderObject() = default;

    void translate(float x, float y, float z);
    void scale(float x, float y, float z);
    void rotate(float angle, float x, float y, float z);

    void render(QOpenGLShaderProgram *shader);

    QMatrix4x4 getModelMatrix() const;
    QVector3D getPosition() const;

protected:
    QMatrix4x4 m_m;

private:
    const int m_numTris;

    GLuint m_vao;
    GLuint m_vbo;
    GLuint m_nbo;
    GLuint m_cbo;
    GLuint m_ibo;
};

#endif // RENDEROBJECT_H
