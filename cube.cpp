#include "cube.h"

const unsigned int numTris = 12;
const unsigned int numVerts = 24;

const GLfloat vertexPosition[3 * numVerts] =
{
    // front
    -CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE, // bottom left
    CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE, // bottom right
    -CUBE_SIZE, CUBE_SIZE, CUBE_SIZE, // top left
    CUBE_SIZE, CUBE_SIZE, CUBE_SIZE, // top right

    // back
    CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    -CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,
    -CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,

    // left
    -CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    -CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE,
    -CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,
    -CUBE_SIZE, CUBE_SIZE, CUBE_SIZE,

    // right
    CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE,
    CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    CUBE_SIZE, CUBE_SIZE, CUBE_SIZE,
    CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,

    // up
    -CUBE_SIZE, CUBE_SIZE, CUBE_SIZE,
    CUBE_SIZE, CUBE_SIZE, CUBE_SIZE,
    -CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,
    CUBE_SIZE, CUBE_SIZE, -CUBE_SIZE,

    // down
    CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    -CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
    CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE,
    -CUBE_SIZE, -CUBE_SIZE, CUBE_SIZE,
};

const GLuint vertexIndex[3 * numTris] =
{
    // front
    0, 1, 3,
    0, 2, 3,

    // back
    4, 5, 7,
    4, 6, 7,

    // left
    8, 9, 11,
    8, 10, 11,

    // right
    12, 13, 15,
    12, 14, 15,

    // up
    16, 17, 19,
    16, 18, 19,

    // down
    20, 21, 23,
    20, 22, 23,
};

const GLfloat normals[3 * numVerts] =
{
    // front
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,

    // back
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,

    // left
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,

    // right
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,

    // up
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,

    // down
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
};

const GLfloat colors[3 * numVerts] =
{
    // front
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,

    // back
    1.0, 1.0, 0.0,
    1.0, 1.0, 0.0,
    1.0, 1.0, 0.0,
    1.0, 1.0, 0.0,

    // left
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,

    // right
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,

    // up
    1.0, 0.0, 1.0,
    1.0, 0.0, 1.0,
    1.0, 0.0, 1.0,
    1.0, 0.0, 1.0,

    // down
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
};

Cube::Cube(QOpenGLFunctions_4_4_Core *gl)
    : RenderObject(gl, numTris, numVerts, vertexPosition,
                   vertexIndex, normals, colors) { }
