#include "helloglwidget.h"

#ifdef DEBUG_SHADOW_MAP
const unsigned int numTris = 2;
const unsigned int numVerts = 4;

const GLfloat vertexPosition[3 * numVerts] =
{
    -1.0, -1.0, 0.0,
    -1.0, 1.0, 0.0,
    1.0, -1.0, 0.0,
    1.0, 1.0, 0.0
};

const GLuint vertexIndex[3 * numTris] =
{
    0, 1, 2,
    1, 3, 2
};
#endif

HelloGLWidget::HelloGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
    m_timer.setInterval(1000 / 30);
    connect(&m_timer, &QTimer::timeout, this, &HelloGLWidget::handleTimeout);
}

void HelloGLWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_W:
        m_wDown = true;
        break;
    case Qt::Key_A:
        m_aDown = true;
        break;
    case Qt::Key_S:
        m_sDown = true;
        break;
    case Qt::Key_D:
        m_dDown = true;
        break;
    case Qt::Key_X:
        m_xDown = true;
        break;
    case Qt::Key_Space:
        m_spaceDown = true;
        break;
    }
}

void HelloGLWidget::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_W:
        m_wDown = false;
        break;
    case Qt::Key_A:
        m_aDown = false;
        break;
    case Qt::Key_S:
        m_sDown = false;
        break;
    case Qt::Key_D:
        m_dDown = false;
        break;
    case Qt::Key_X:
        m_xDown = false;
        break;
    case Qt::Key_Space:
        m_spaceDown = false;
        break;
    }
}

void HelloGLWidget::handleTimeout()
{
    update();
}

void HelloGLWidget::normal()
{
    m_shaderMode = SHADER_MODE_NORMAL;
}

void HelloGLWidget::pcf()
{
    m_shaderMode = SHADER_MODE_PCF;
}

void HelloGLWidget::vsm()
{
    m_shaderMode = SHADER_MODE_VSM;
}

void HelloGLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0.5F, 0.5F, 0.5F, 1.0F);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    m_debugLogger = new DebugLogger();

    m_shadowMapShader = createShader(":/shadowmap.vert", ":/shadowmap.frag");
    m_normalShader = createShader(":/normal.vert", ":/normal.frag");
    m_pcfShader = createShader(":/pcf.vert", ":/pcf.frag");
    m_vsmShader = createShader(":/vsm.vert", ":/vsm.frag");

    m_ground = new Ground(this);

    m_cubes = new Cube*[m_cubeCount];
    m_cubes[0] = new Cube(this);
    m_cubes[0]->translate(-5, CUBE_SIZE * 2, -10);

    m_cubes[1] = new Cube(this);
    m_cubes[1]->translate(5, CUBE_SIZE * 1.2F, -5);
    m_cubes[1]->scale(0.5, 0.5, 0.5);

    m_lightSources = new LightSource*[m_lightSourceCount];
    m_lightSources[0] = new LightSource(this, defaultFramebufferObject(),
                                        QVector3D(CUBE_SIZE, CUBE_SIZE * 10, 0),
                                        m_cubes[0]->getPosition(), 0);
    m_lightSources[1] = new LightSource(this, defaultFramebufferObject(),
                                        QVector3D(CUBE_SIZE * 2, CUBE_SIZE * 5, CUBE_SIZE),
                                        m_cubes[1]->getPosition(), 1);

#ifdef DEBUG_SHADOW_MAP
    m_debugShader = new QOpenGLShaderProgram();
    m_debugShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/debug.vert");
    m_debugShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/debug.frag");
    m_debugShader->link();
    m_debugShader->bind();

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    m_debugShader->enableAttributeArray(0);
    m_debugShader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numTris * sizeof(GLuint), vertexIndex, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
#endif

    m_timer.start();
}

void HelloGLWidget::paintGL()
{
    resize(SHADOW_MAP_SIZE, SHADOW_MAP_SIZE);
    for (size_t i = 0; i < m_lightSourceCount; i++)
    {
        m_lightSources[i]->getShadowMap()->bind();

        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_shadowMapShader->bind();
        m_shadowMapShader->setUniformValue("ls", getLightSpace(m_lightSources[i]));

        render(m_shadowMapShader);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebufferObject());
    }

#ifndef DEBUG_SHADOW_MAP
    resize(width(), height());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_camera.move(m_wDown, m_aDown, m_sDown, m_dDown, m_xDown, m_spaceDown);

    for (size_t i = 0; i < m_lightSourceCount; i++)
    {
        m_lightSources[i]->getShadowMap()->bindTexture();
    }

    QOpenGLShaderProgram *shader = getRenderShader();

    shader->bind();
    shader->setUniformValue("v", m_camera.getViewMatrix());
    shader->setUniformValue("p", m_p);
    shader->setUniformValue("ls0", getLightSpace(m_lightSources[0]));
    shader->setUniformValue("ls1", getLightSpace(m_lightSources[1]));
    shader->setUniformValue("lightPos0", m_lightSources[0]->getPosition());
    shader->setUniformValue("lightPos1", m_lightSources[1]->getPosition());
    shader->setUniformValue("cameraPos", m_camera.getPosition());
    shader->setUniformValue("znear", Z_NEAR);
    shader->setUniformValue("zfar", Z_FAR);

    render(shader);
#else
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_debugShader->bind();
    m_debugShader->setUniformValue("znear", Z_NEAR);
    m_debugShader->setUniformValue("zfar", Z_FAR);
    m_lightSources[0]->getShadowMap()->bindTexture();
    glBindVertexArray(m_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glDrawElements(GL_TRIANGLES, 3 * numTris, GL_UNSIGNED_INT, nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
#endif
}

void HelloGLWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_lastMousePos = event->pos();
        m_mousePressed = true;
    }
}

void HelloGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_mousePressed = false;
    }
}

void HelloGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mousePressed)
    {
        m_camera.rotate(QVector2D(m_lastMousePos), QVector2D(event->pos()));
        m_lastMousePos = event->pos();
    }
}

void HelloGLWidget::resize(int width, int height)
{
    glViewport(0, 0, width, height);
    m_p.setToIdentity();
    m_p.perspective(45.0F, GLfloat(width) / height, Z_NEAR, Z_FAR);
}

QOpenGLShaderProgram* HelloGLWidget::createShader(const QString &vertName, const QString &fragName)
{
    QOpenGLShaderProgram *shader = new QOpenGLShaderProgram();
    shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vertName);
    shader->addShaderFromSourceFile(QOpenGLShader::Fragment, fragName);
    shader->link();
    return shader;
}

QOpenGLShaderProgram* HelloGLWidget::getRenderShader()
{
    if (m_shaderMode == SHADER_MODE_PCF)
        return m_pcfShader;

    if (m_shaderMode == SHADER_MODE_VSM)
        return m_vsmShader;

    return m_normalShader;
}

QMatrix4x4 HelloGLWidget::getLightSpace(LightSource *lightSource) const
{
    QMatrix4x4 lp;
    lp.perspective(45.0F, 1, Z_NEAR, Z_FAR);
    return lp * lightSource->getViewMatrix();
}

void HelloGLWidget::render(QOpenGLShaderProgram *shader)
{
    m_ground->render(shader);

    for (size_t i = 0; i < m_cubeCount; i++)
    {
        m_cubes[i]->render(shader);
    }
}
