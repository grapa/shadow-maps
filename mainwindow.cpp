#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindowTitle("Shadow Maps");
    resize(1280, 720);

    m_glWidget = new HelloGLWidget();
    setCentralWidget(m_glWidget);

    QToolBar *toolBar = new QToolBar();
    addToolBar(Qt::TopToolBarArea, toolBar);

    QAction *normalAction = toolBar->addAction("Normal");
    normalAction->setCheckable(true);
    normalAction->setChecked(true);

    QAction *pcfAction = toolBar->addAction("PCF");
    pcfAction->setCheckable(true);

    QAction *vsmAction = toolBar->addAction("VSM");
    vsmAction->setCheckable(true);

    QActionGroup *shadingGroup = new QActionGroup(toolBar);
    shadingGroup->addAction(normalAction);
    shadingGroup->addAction(pcfAction);
    shadingGroup->addAction(vsmAction);

    connect(normalAction, &QAction::triggered, m_glWidget, &HelloGLWidget::normal);
    connect(pcfAction, &QAction::triggered, m_glWidget, &HelloGLWidget::pcf);
    connect(vsmAction, &QAction::triggered, m_glWidget, &HelloGLWidget::vsm);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    m_glWidget->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    m_glWidget->keyReleaseEvent(event);
}
