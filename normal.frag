#version 440

in vec3 worldPos_FS;
in vec3 worldNormal_FS;
in vec4 lightSpacePos0_FS;
in vec4 lightSpacePos1_FS;
in vec3 color_FS;

out vec4 frag;

uniform vec3 lightPos0;
uniform vec3 lightPos1;
uniform vec3 cameraPos;
uniform float znear;
uniform float zfar;

layout(binding = 0) uniform sampler2D shadowMap0;
layout(binding = 1) uniform sampler2D shadowMap1;

float linearize(float depth)
{
    return (-zfar * znear / (depth * (zfar - znear) - zfar)) / zfar;
}

float calcShadow(vec4 lightSpacePos, vec3 lightPos, sampler2D shadowMap)
{
    vec3 projCoords = lightSpacePos.xyz / lightSpacePos.w;
    projCoords = (projCoords + 1) / 2;
    vec2 uv = projCoords.xy;
    if (uv.x < 0 || uv.x > 1 || uv.y < 0 || uv.y > 1)
        return 0;

    float closestDepth = texture(shadowMap, uv).r;
    float currentDepth = projCoords.z;
    float bias = max(0.05 * (1.0 - dot(normalize(worldNormal_FS), normalize(lightPos - worldPos_FS))), 0.001);
    return (linearize(currentDepth) - bias > linearize(closestDepth)) ? 1 : 0;
}

vec3 computeLighting(vec3 lightColor, vec3 normal, vec4 lightSpacePos, vec3 lightPos, sampler2D shadowMap)
{
    vec3 lightDir = normalize(lightPos - worldPos_FS);
    float diff = max(dot(lightDir, normal), 0);
    vec3 diffuse = diff * lightColor;

    vec3 viewDir = normalize(cameraPos - worldPos_FS);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0), 64);
    vec3 specular = spec * lightColor;

    float shadow = calcShadow(lightSpacePos, lightPos, shadowMap);
    return (1 - shadow) * (diffuse + specular);
}

void main(void)
{
    vec3 normal = normalize(worldNormal_FS);
    vec3 lightColor = vec3(1);
    vec3 ambient = 0.1 * lightColor;

    vec3 lighting0 = computeLighting(lightColor, normal, lightSpacePos0_FS, lightPos0, shadowMap0);
    vec3 lighting1 = computeLighting(lightColor, normal, lightSpacePos1_FS, lightPos1, shadowMap1);
    vec3 lighting = (ambient + (lighting0 + lighting1) / 2) * color_FS;

    frag = vec4(lighting, 1);
}
