#include "lightsource.h"

LightSource::LightSource(QOpenGLFunctions_4_4_Core *gl, GLuint defaultFBO,
                         QVector3D position, QVector3D lookAt, unsigned int index)
    : SceneObject(gl), m_position(position), m_lookAt(lookAt),
      m_shadowMap(new ShadowMap(gl, defaultFBO, index)) { }

QVector3D LightSource::getPosition() const
{
    return m_position;
}

QMatrix4x4 LightSource::getViewMatrix() const
{
    QMatrix4x4 v;
    v.lookAt(m_position, m_lookAt, QVector3D(0, 0, -1));
    return v;
}

ShadowMap* LightSource::getShadowMap() const
{
    return m_shadowMap;
}
