#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#define SHADOW_MAP_SIZE 1024

#include "sceneobject.h"

class ShadowMap : public SceneObject
{
public:
    ShadowMap(QOpenGLFunctions_4_4_Core *gl, GLuint defaultFBO, unsigned int index);
    ~ShadowMap() = default;

    void bind();
    void bindTexture();
private:
    GLuint m_fbo;
    GLuint m_texture;
    unsigned int m_index;
};

#endif // SHADOWMAP_H
