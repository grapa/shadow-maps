#include "camera.h"

Camera::Camera(QVector3D point)
     : m_position(point + QVector3D(2, 0.5F, 3)),
       m_lookAt(point) { }

QMatrix4x4 Camera::getViewMatrix() const
{
    QMatrix4x4 v;
    v.lookAt(m_position, m_lookAt, m_up);
    return v;
}

void Camera::move(bool w, bool a, bool s, bool d, bool x, bool space)
{
    QVector3D direction = (m_lookAt - m_position);
    direction.normalize();
    QVector3D right = QVector3D::crossProduct(direction, m_up).normalized();

    if (w)
    {
        m_position += direction * m_moveSpeed;
        m_lookAt += direction * m_moveSpeed;
    }

    if (a)
    {
        m_position -= right * m_moveSpeed;
        m_lookAt -= right * m_moveSpeed;
    }

    if (s)
    {
        m_position -= direction * m_moveSpeed;
        m_lookAt -= direction * m_moveSpeed;
    }

    if (d)
    {
        m_position += right * m_moveSpeed;
        m_lookAt += right * m_moveSpeed;
    }

    if (x)
    {
        m_position -= m_up * m_moveSpeed;
        m_lookAt -= m_up * m_moveSpeed;
    }

    if (space)
    {
        m_position += m_up * m_moveSpeed;
        m_lookAt += m_up * m_moveSpeed;
    }
}

void Camera::rotate(QVector2D from, QVector2D to)
{
    QVector2D delta = to - from;
    QMatrix4x4 rotation;
    rotation.rotate(-delta.x() * m_mouseSpeed * 50, 0, 1);
    rotation.rotate(-delta.y() * m_mouseSpeed * 50, 1, 0);
    m_position = rotation * (m_position - m_lookAt) + m_lookAt;
}

QVector3D Camera::getPosition() const
{
    return m_position;
}
